package com.company;
import java.util.Random;

/**
 * Created by Patryk on 10/19/2016.
 */

public class Perceptron {

    private double[] wagi;
    private double progWejscia;

    public void Trenuj (double [][] wejscie, int [] wyjscie, double prog, double lrate, double generacja){

        this.progWejscia = prog;
        int n = wejscie[0].length;
        int p = wyjscie.length;
        wagi = new double [n];
        Random r = new Random();

        //inicjalizacja wag
        for (int i = 0; i<n; i++){
            wagi[i] = r.nextDouble();
        }

        //kolejne generacje
        for (int i = 0; i< generacja; i++){

            int bladCalkowity = 0;
            for (int j = 0; j< p; j++){

                int wyjsciePojedyncze = Wyjscie(wejscie[j]);
                int blad = wyjscie[j] - wyjsciePojedyncze;

                bladCalkowity += blad;

                for (int k = 0; k < n; k++){
                    double delta = lrate * wejscie[j][k] * blad;
                    wagi[k] += delta;
                }
            }
            if (bladCalkowity == 0)
                break;
        }

        for (int i = 0; i<wagi.length; i++){
            System.out.println("Waga nr " +  i + " wynosi " + wagi[i]);
        }
    }

    public int Wyjscie (double [] wejscie){
        double suma = 0.0;

        for (int i = 0; i<wejscie.length; i++){
            suma += wejscie[i] * wagi[i];
        }

        if (suma>progWejscia)
            return 1;
        else
            return 0;
    }
}
