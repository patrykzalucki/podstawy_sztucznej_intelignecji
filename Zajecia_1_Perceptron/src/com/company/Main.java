package com.company;
public class Main {

    public static void main(String[] args) {

        Perceptron p = new Perceptron();
        double wejscia[][] = {{0, 0}, {0, 1}, {1, 0}, {1, 1}};
        int wyjscia[] = {0, 1, 1, 1};

        p.Trenuj(wejscia, wyjscia, 0.2, 0.1, 200);
        System.out.println(p.Wyjscie(new double[]{0, 0}));
        System.out.println(p.Wyjscie(new double[]{1, 0}));
        System.out.println(p.Wyjscie(new double[]{0, 1}));
        System.out.println(p.Wyjscie(new double[]{1, 1}));
    }
}
